<?php

namespace App;

use Illuminate\Foundation\Application as ApplicationOriginal;

class Application extends ApplicationOriginal
{
    public function publicPath()
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'public_laravel';
    }
}
