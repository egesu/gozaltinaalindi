<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Tweet;
use Carbon\Carbon;
use Abraham\TwitterOAuth\TwitterOAuth;
use App\Facades\Twitter;

class FetchOldTweetsCommand extends Command
{
	protected $signature = 'twitter:fetch-old';

	public function handle()
	{
		// $firstTweet = Tweet::orderBy('tweet_id', 'asc')->first();
		$addedCount = 0;

		$next = null;
		if(file_exists('next.txt')) {
			$next = file_get_contents('next.txt');
		}

		try {
			$data = [
				'query' => '"gözaltına alındı"',
				'maxResults' => 100,
				'fromDate' => '201501010000',
				'toDate' => '201801010000',
			];
			if(!empty($next)) {
				$data['next'] = $next;
			}
			$results = Twitter::getSearchFullArchive('dev', $data);

			foreach($results->results as $status) {
				if(substr($status->text, 0, 4) === 'RT @') {
					continue; // is retweet
				}
				if(!Tweet::where('tweet_id', $status->id)->first()) {
					Tweet::create([
						'tweet_id' => $status->id,
				    'twitter_user_id' => $status->user->id,
				    'retweeted_id' => isset($status->retweeted_status) ? $status->retweeted_status->id : null,
				    'quoted_tweet_id' => isset($status->quoted_status_id) ? $status->quoted_status_id : null,
				    'tweet_text' => $status->text,
				    'sent_at' => Carbon::parse($status->created_at)->setTimezone('Europe/Istanbul'),
				    'retweet_count' => $status->retweet_count,
				    'favorite_count' => $status->favorite_count,
				    'retweets_checked' => true,
				    'not_available' => false,
					]);
					$addedCount++;
				}
			}

			if(!empty($results->next)) {
				file_put_contents('next.txt', $results->next);
			}
		} catch(RunTimeException $e) {
			dd($e);
		}

		$this->line("$addedCount tweets added");

		// echo "hello";
		// return true;
		//
		// $params = [
		// 	'query' => '"gözaltına alındı"',
		// 	'maxResults' => 100,
		// 	'toDate' => $firstTweet->sent_at->setTimezone('UTC')->format('YmdHi'),
		// 	// 'toDate' => '201801110928',
		// ];
		// $paramValues = [];
		//
		// foreach($params as $key => $value) {
		// 	$paramValues[] = "$key=" . str_replace('"', '\"', $value);
		// }
		//
		// $cmd = 'twurl "/1.1/tweets/search/30day/local.json?' . implode('&', $paramValues) . '"';
		// $statuses = json_decode(shell_exec($cmd));
		//
		// $addedCount = 0;
		//
		// if(!isset($statuses->results)) {
		// 	dd($statuses);
		// }
		//
		// foreach($statuses->results as $status) {
		// 	if(isset($status->retweeted_status) and $status->is_quote_status === false) {
		// 		$this->line('Skipping retweet');
		// 		continue;
		// 	}
		//
		// 	if(!Tweet::where('tweet_id', $status->id)->first()) {
		// 		Tweet::create([
		// 			'tweet_id' => $status->id,
		// 	    'twitter_user_id' => $status->user->id,
		// 	    'quoted_tweet_id' => isset($status->retweeted_status) ? $status->retweeted_status->id : null,
		// 	    'tweet_text' => $status->text,
		// 	    'sent_at' => Carbon::parse($status->created_at)->setTimezone('Europe/Istanbul'),
		// 	    'retweet_count' => $status->retweet_count,
		// 	    'favorite_count' => $status->favorite_count,
		// 		]);
		// 		$addedCount++;
		// 	}
		// }
		//
		// $this->line("$addedCount tweets added");
	}
}
