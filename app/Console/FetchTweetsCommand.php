<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Tweet;
use Carbon\Carbon;
use Thujohn\Twitter\Facades\Twitter;

class FetchTweetsCommand extends Command
{
	protected $signature = 'twitter:fetch';

	public function handle()
	{
		$firstTweet = Tweet::orderBy('tweet_id', 'desc')->first();
		$results = Twitter::getSearch([
			'q' => '"gözaltına alındı" -filter:retweets',
			'result_type' => 'recent',
			'count' => 100,
			'include_entities' => false,
			'since_id' => $firstTweet ? $firstTweet->tweet_id : null,
			// 'max_id' => $firstTweet ? $firstTweet->tweet_id : null,
		]);

		$addedCount = 0;

		foreach($results->statuses as $status) {
			if(substr($status->text, 0, 4) === 'RT @') {
				continue; // is retweet
			}
			if(!Tweet::where('tweet_id', $status->id)->first()) {
				Tweet::create([
					'tweet_id' => $status->id,
			    'twitter_user_id' => $status->user->id,
			    'retweeted_id' => isset($status->retweeted_status) ? $status->retweeted_status->id : null,
			    'quoted_tweet_id' => isset($status->quoted_status_id) ? $status->quoted_status_id : null,
			    'tweet_text' => $status->text,
			    'sent_at' => Carbon::parse($status->created_at)->setTimezone('Europe/Istanbul'),
			    'retweet_count' => $status->retweet_count,
			    'favorite_count' => $status->favorite_count,
				]);
				$addedCount++;
			}
		}

		$this->line("$addedCount tweets added");
	}
}
