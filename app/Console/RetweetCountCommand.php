<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Tweet;
use Carbon\Carbon;
use Thujohn\Twitter\Facades\Twitter;

class RetweetCountCommand extends Command
{
	protected $signature = 'twitter:retweets';

	public function handle()
	{
		$count = 0;
		$tweetIds = Tweet::where('retweets_checked', false)
			->where('not_available', false)
			->where('sent_at', '<', Carbon::parse('-1 week'))
			->orderBy('id', 'asc')
			->limit(100)
			->pluck('tweet_id')
			->toArray();

		$results = Twitter::getStatusesLookup([
			'id' => implode(',', $tweetIds),
			'include_entities' => false,
			'trim_user' => true,
		]);

		$notAvailableIds = $tweetIds;

		foreach($results as $status) {
			$index = array_search($status->id, $tweetIds);
			unset($notAvailableIds[$index]);

			Tweet::where('tweet_id', $status->id)
				->update([
					'retweet_count' => $status->retweet_count,
					'favorite_count' => $status->favorite_count,
					'retweets_checked' => true,
					'sent_at' => Carbon::parse($status->created_at)->setTimezone('Europe/Istanbul'),
				]);

			$count++;
		}

		if(count($notAvailableIds) > 0) {
			Tweet::whereIn('tweet_id', $notAvailableIds)
				->update([
					'not_available' => true,
				]);
		}

		$this->line("$count tweets updated, " . count($notAvailableIds) . " not available");
	}
}
