<?php

namespace App\Http\Controllers\Api;

use App\Models\Tweet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;

class TweetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $startDate = $request->input('startDate');
        $endDate = $request->input('endDate');

        $query = Tweet::query()
            ->select([
                DB::raw('DISTINCT DATE(sent_at) AS tweet_sent_at'),
                DB::raw('COUNT(*) AS tweet_count'),
                DB::raw('SUM(retweet_count) AS retweet_count'),
            ])
            ->groupBy(DB::raw('DATE(sent_at)'))
            ->orderBy(DB::raw('1'), 'ASC');

        if($startDate) {
            $query = $query->where(DB::raw('DATE(sent_at)'), '>=', $startDate);
        }

        if($endDate) {
            $query = $query->where(DB::raw('DATE(sent_at)'), '<=', $endDate);
        }

        return $query->get()->toArray();
    }

    public function getHotForDate(Request $request)
    {
        $date = $request->input('date');
        return Tweet::where(DB::raw('DATE(sent_at)'), $date)
            ->orderBy('retweet_count', 'DESC')
            ->limit(5)
            ->get()->toArray();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tweet  $tweet
     * @return \Illuminate\Http\Response
     */
    public function show(Tweet $tweet)
    {
        //
    }
}
