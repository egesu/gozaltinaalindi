<?php

namespace App\Libraries\Traits;

trait Search30DaysTrait
{
  public function getSearch30Days($environment, $parameters = [])
  {
    if (!array_key_exists('query', $parameters))
    {
      throw new BadMethodCallException('Parameter required missing : query');
    }

    return $this->post('tweets/search/30day/' . $environment, $parameters);
  }
}
