<?php

namespace App\Libraries;

use Thujohn\Twitter\Twitter as TwitterOriginal;
use BadMethodCallException;

class Twitter extends TwitterOriginal
{
  public function getSearch30Days(string $environment, $parameters = [])
  {
    if (!array_key_exists('query', $parameters)) {
      throw new BadMethodCallException('Parameter required missing : query');
    }

    return $this->post('tweets/search/30day/' . $environment, $parameters);
  }

  public function getSearchFullArchive(string $environment, $parameters = [])
  {
    if (!array_key_exists('query', $parameters)) {
      throw new BadMethodCallException('Parameter required missing : query');
    }

    return $this->get('tweets/search/fullarchive/' . $environment, $parameters);
  }
}
