<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
  protected $table = 'tweets';

  protected $dates = [
    'created_at',
    'updated_at',
    'sent_at',
  ];

  protected $fillable = [
    'tweet_id',
    'twitter_user_id',
    'retweeted_id',
    'quoted_tweet_id',
    'tweet_text',
    'sent_at',
    'retweet_count',
    'favorite_count',
    'retweets_checked',
    'not_available',
  ];

  public $casts = [
    'retweets_checked' => 'boolean',
    'not_available' => 'boolean',
    'tweet_id' => 'string',
  ];
}
