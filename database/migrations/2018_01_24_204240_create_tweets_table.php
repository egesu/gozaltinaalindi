<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTweetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tweets', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('tweet_id');
            $table->bigInteger('twitter_user_id');
            $table->bigInteger('retweeted_id')->nullable();
            $table->bigInteger('quoted_tweet_id')->nullable();
            $table->integer('retweet_count')->default(0);
            $table->integer('favorite_count')->default(0);
            $table->string('tweet_text', 500);
            $table->timestamp('sent_at')->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tweets');
    }
}
