import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../environments/environment';

interface DateTweetObject {
  retweet_count: string;
  tweet_count: number;
  tweet_sent_at: Date;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  preserveWhitespaces: false,
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit {
  public title = 'app';
  public list: DateTweetObject[];
  public results = [];
  public hotTweets: any[];
  public isSidemenuActive = false;
  public data = {
    startDate: null,
    endDate: null,
  };

  constructor(
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.loadGraphData();
  }

  public loadGraphData() {
    let params: any = {};

    if (this.data.startDate) {
      params.startDate = this.data.startDate;
    }

    if (this.data.endDate) {
      params.endDate = this.data.endDate;
    }

    this.results = [];

    this.http.get(environment.apiBasePath + 'tweets', { params: params }).toPromise()
      .then((response: DateTweetObject[]) => {
        this.list = response;

        for (const item of response) {
          this.results.push({
            name: (new Date(item.tweet_sent_at)).toLocaleDateString(),
            series: [
              {
                name: "Tweet sayısı",
                value: item.tweet_count,
              },
              {
                name: "Retweet sayısı",
                value: parseInt(item.retweet_count, 10),
              },
            ],
          });
        }
      });
  }

  public onSelect(selectedItem) {
    this.hotTweets = [];
    const date = (new Date(selectedItem.series));
    const dateString = date.getFullYear() + '-' + (date.getMonth() + 1).toString().padStart(2, '0') + '-' + date.getDate().toString().padStart(2, '0');
    this.http.get(environment.apiBasePath + 'tweets/hot?date=' + dateString).toPromise()
      .then((response: any[]) => {
        for (const item of response) {
          item.dateJs = new Date(item.sent_at);
          this.hotTweets.push(item);
        }
      });
  }

  public handleFormSubmit() {
    this.loadGraphData();
  }

  public toggleSidemenu(event) {
    this.isSidemenuActive = !this.isSidemenuActive;
  }
}
